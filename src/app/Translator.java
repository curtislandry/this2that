package app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Translator {
    public Translator(){}

    public boolean translate(File source, File dest) {
        
        try (BufferedReader reader = new BufferedReader(new FileReader(source))) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(dest))) {

                String line;
                while ((line = reader.readLine()) != null) {
                    writer.write(line);
                    writer.newLine();
                }
            } catch (IOException ex) {
                logException(ex);
                return false;
            }
        } catch (IOException e) {
            logException(e);
            return false;
        }
        return true;
    }

    private void logException(Exception e) {
        if (e == null) {
            return;
        }
        System.out.println(e.getMessage());
        System.out.println(e.getStackTrace());
    }

}