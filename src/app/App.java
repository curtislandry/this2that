package app;

import java.io.File;

public class App {
    public static void main(String[] args) throws Exception {
        Translator tran = new Translator();
        tran.translate(new File("input.txt"), new File("output.txt"));
    }
}